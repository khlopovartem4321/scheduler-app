package com.pr.respapp

import android.app.Application
import com.pr.respapp.data.api.ScheduleApi
import com.pr.respapp.data.repository.ScheduleRepositoryImpl
import com.pr.respapp.domain.repository.ScheduleRepository
import com.pr.respapp.presentation.pages.main.MainPageViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

val appModule = module {
    single { ScheduleApi() }
    single<ScheduleRepository> { ScheduleRepositoryImpl(get()) }
    single { MainPageViewModel(get()) }
}

class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MyApplication)
            modules(appModule)
        }
    }
}