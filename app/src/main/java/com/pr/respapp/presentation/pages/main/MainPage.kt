package com.pr.respapp.presentation.pages.main

import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.compose.koinInject


@Composable
fun MainPage(vm: MainPageViewModel = koinInject()) {

    val coroutineScope = rememberCoroutineScope();

    LaunchedEffect(key1 = true, block = {
        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                vm.getSchedules()
            }
        }
    })

    Row {

    }
}