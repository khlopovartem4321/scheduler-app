package com.pr.respapp.presentation.widgets

import android.content.Context
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.capitalize
import androidx.compose.ui.unit.dp
import androidx.glance.Button
import androidx.glance.GlanceId
import androidx.glance.GlanceModifier
import androidx.glance.LocalContext
import androidx.glance.action.clickable
import androidx.glance.appwidget.GlanceAppWidget
import androidx.glance.appwidget.lazy.LazyColumn
import androidx.glance.appwidget.provideContent
import androidx.glance.background
import androidx.glance.layout.Column
import androidx.glance.layout.Row
import androidx.glance.layout.Spacer
import androidx.glance.layout.fillMaxSize
import androidx.glance.layout.fillMaxWidth
import androidx.glance.layout.height
import androidx.glance.layout.padding
import androidx.glance.layout.width
import androidx.glance.text.Text
import androidx.glance.text.TextAlign
import androidx.glance.text.TextStyle
import com.pr.respapp.domain.model.ScheduleWithReplacement
import com.pr.respapp.presentation.pages.main.MainPageViewModel
import com.pr.respapp.utils.dates
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.compose.koinInject
import java.util.Locale

class ScheduleWidget : GlanceAppWidget() {
    override suspend fun provideGlance(context: Context, id: GlanceId) {
        provideContent {
            Schedules()
        }
    }
}

@Composable
fun Schedules(vm: MainPageViewModel = koinInject()) {
    val coroutineScope = rememberCoroutineScope()
    val schedule = remember {
        mutableStateOf(ScheduleWithReplacement(listOf(), listOf(), true, 1))
    }
    val re = remember {
        mutableIntStateOf(0)
    }
    val context = LocalContext.current

    LaunchedEffect(key1 = re.intValue) {
        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                schedule.value = vm.getSchedules()
                withContext(Dispatchers.Main) {
                    Toast.makeText(context, "Обновлено", Toast.LENGTH_LONG).show()
                }
            }
        }
    }


    Column(modifier = GlanceModifier.background(Color.White).padding(5.dp)) {
        Text(
            text = dates[schedule.value.day - 1].replaceFirstChar { it.uppercase() },
            style = TextStyle(textAlign = TextAlign.Center),
            modifier = GlanceModifier.fillMaxWidth()
        )
        Text(
            text = if (schedule.value.isReplacementCorrect) "Замены доступны" else "Замены недоступны",
            modifier = GlanceModifier.fillMaxWidth().clickable {
                re.intValue++;
            },
            style = TextStyle(textAlign = TextAlign.Center)
        )

        Spacer(modifier = GlanceModifier.height(5.dp))

        LazyColumn(modifier = GlanceModifier.fillMaxSize()) {
            items(schedule.value.schedule.size) { index ->
                val sch = schedule.value.schedule[index]
                val time = schedule.value.time[index]


                Row(modifier = GlanceModifier.fillMaxWidth()) {
                    Text(
                        text = time,
                        modifier = GlanceModifier.defaultWeight(),
                        style = TextStyle(textAlign = TextAlign.Center)
                    )
                    Spacer(modifier = GlanceModifier.width(5.dp))
                    Text(
                        text = if (sch.replacementItem == null) sch.subject else "${sch.replacementItem.subject} ${sch.replacementItem.cabinet}",
                        modifier = GlanceModifier.defaultWeight(),
                        style = TextStyle(textAlign = TextAlign.Center)
                    )
                }
            }
        }
    }
}
