package com.pr.respapp.presentation.pages.main


import androidx.lifecycle.ViewModel
import com.pr.respapp.domain.model.ScheduleWithReplacement
import com.pr.respapp.domain.repository.ScheduleRepository
import java.util.Calendar

class MainPageViewModel(private val scheduleRepository: ScheduleRepository): ViewModel() {
    suspend fun getSchedules(): ScheduleWithReplacement {
        val day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)

        return scheduleRepository.getCurrentSchedule(if (day != Calendar.SUNDAY) day - 1 else day, 313)
    }
}