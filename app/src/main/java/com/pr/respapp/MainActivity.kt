package com.pr.respapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.pr.respapp.data.api.ScheduleApi
import com.pr.respapp.data.repository.ScheduleRepositoryImpl
import com.pr.respapp.domain.repository.ScheduleRepository
import com.pr.respapp.presentation.pages.main.MainPage
import com.pr.respapp.presentation.pages.main.MainPageViewModel
import com.pr.respapp.presentation.ui.theme.RespAppTheme
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RespAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainPage()
                }
            }
        }
    }
}
