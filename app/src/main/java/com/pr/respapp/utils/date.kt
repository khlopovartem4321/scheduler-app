package com.pr.respapp.utils


val dates = listOf("понедельник", "вторник", "среда", "четверг", "пятница", "суббота")

fun mapDate(date: String): Number {
    val d = date.lowercase()

    return dates.indexOf(d)
}