package com.pr.respapp.data.api

import android.util.Log
import com.pr.respapp.data.entity.ReplacementItem
import com.pr.respapp.utils.mapDate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import org.jsoup.select.Elements

class ScheduleApi {
    fun getCurrentSchedule(day: Int, group: Int): Pair<List<String>, List<String>> {
        val connection =
            Jsoup.connect("https://spb-kit.ru/studentam/raspisanie_zanyatiy_zameny/")
                ?: throw Error("Сервис недоступен")

        val document = connection.get()

        val groupContainer = document.select(".schedule-block-main-content").first { element ->
            element.attr("data-tab").equals(group.toString())
        }!!
        val allSchedule = groupContainer.select(".schedule-table-column").map {
            it.select(".schedule-table-row")
        }.filterIndexed { index, _ ->
            index > 0
        }

        return Pair(
            parseScheduleColumn(allSchedule, 0),
            parseScheduleColumn(allSchedule, day),
        )
    }

    private fun parseScheduleColumn(elements: List<Elements>, index: Int): List<String> {
        val scheduleList = arrayListOf<String>()
        for (element in elements) {
            if (element[index].childrenSize() == 0) {
                scheduleList.add("Пусто")
            } else {
                scheduleList.add(element[index].children().first().text())
            }
        }

        return scheduleList
    }


    fun getReplacement(day: Int, group: Int): List<ReplacementItem> {
        val connection =
            Jsoup.connect("http://rep.spb-kit.online:8005/replacements/api/fetch-rep?ts=${System.currentTimeMillis()}")
                ?: throw Exception("Сервис недоступен")
        val document = connection.get()
        val tables = document.select("tr")

        if (mapDate(tables[0].selectFirst("td").text().split(" ")[1]) != day - 1) {
            throw Exception("День не совпадает")
        }


        var startIndex = -1
        for ((index, element) in tables.withIndex()) {
            val sel = element.selectFirst(".section") ?: continue

            if (sel.text().equals(group.toString())) {
                startIndex = index
                break
            }
        }

        if (startIndex == -1) {
            return listOf()
        }

        var index = startIndex + 2
        val replacement: ArrayList<ReplacementItem> = arrayListOf()

        while (index < tables.size) {
            val el = tables[index].select(".content")

            if (el.size == 0) {
                break
            }

            replacement.add(ReplacementItem(el[0].text().toInt(), el[3].text(), el[2].text(), el[4].text()))

            index++
        }


        return replacement
    }
}