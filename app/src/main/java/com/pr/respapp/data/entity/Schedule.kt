package com.pr.respapp.data.entity

data class Schedule(val time: List<String>, val day: Int, val schedule: List<String>)