package com.pr.respapp.data.repository

import android.util.Log
import com.pr.respapp.data.api.ScheduleApi
import com.pr.respapp.data.entity.ReplacementItem
import com.pr.respapp.data.entity.Schedule
import com.pr.respapp.data.entity.toReplacement
import com.pr.respapp.domain.model.ScheduleItemWithReplacement
import com.pr.respapp.domain.model.ScheduleWithReplacement
import com.pr.respapp.domain.repository.ScheduleRepository
import java.lang.Exception

class ScheduleRepositoryImpl(private val scheduleApi: ScheduleApi) : ScheduleRepository {
    override suspend fun getCurrentSchedule(day: Int, group: Int): ScheduleWithReplacement {
        val schedule = scheduleApi.getCurrentSchedule(day, group)

        return try {
            ScheduleWithReplacement(
                schedule.first,
                mergeSchedule(schedule.second, scheduleApi.getReplacement(day, group)),
                true,
                day
            )
        } catch (e: Exception) {
            ScheduleWithReplacement(
                schedule.first,
                mergeSchedule(schedule.second, listOf()),
                false,
                day
            )
        }
    }

    private fun mergeSchedule(
        currentSchedule: List<String>,
        replacement: List<ReplacementItem>
    ): List<ScheduleItemWithReplacement> {
        val list = arrayListOf<ScheduleItemWithReplacement>()

        for ((index, subject) in currentSchedule.withIndex()) {
            list.add(
                ScheduleItemWithReplacement(
                    subject,
                    toReplacement(replacement.find { index + 1 == it.index })
                )
            )
        }

        return list
    }
}