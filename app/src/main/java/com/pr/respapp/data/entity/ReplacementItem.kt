package com.pr.respapp.data.entity

import com.pr.respapp.domain.model.Replacement

data class ReplacementItem(val index: Int, val name: String, val teacher: String, val cabinet: String)

fun toReplacement(replacement: ReplacementItem?): Replacement? {
    if(replacement == null) {
        return null
    }
    return Replacement(replacement.name, replacement.teacher, replacement.cabinet)
}