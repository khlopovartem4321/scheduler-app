package com.pr.respapp.domain.repository

import com.pr.respapp.domain.model.ScheduleWithReplacement

interface ScheduleRepository {
    suspend fun getCurrentSchedule(day: Int, group: Int): ScheduleWithReplacement
}