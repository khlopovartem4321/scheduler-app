package com.pr.respapp.domain.model


data class ScheduleWithReplacement(val time: List<String>, val schedule: List<ScheduleItemWithReplacement>, val isReplacementCorrect: Boolean, val day: Int)