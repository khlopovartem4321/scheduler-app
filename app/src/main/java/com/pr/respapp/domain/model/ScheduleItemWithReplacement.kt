package com.pr.respapp.domain.model


data class Replacement(val subject: String, val teacher: String, val cabinet: String)
data class ScheduleItemWithReplacement(val subject: String, val replacementItem: Replacement?)